# MenuTitle: select glyphOrder file (do not create empty glyphs)

__doc__ = """\
    Set the current font glyphOrder from a .txt/.enc file with the ordered list of glyphs.
    It doesn't create empty glyphs for glyphs not existing in the font.

    Needs vanilla module installed for dialog.
"""

from vanilla.dialogs import getFile


def getGlyphOrderFile():
    goFile = getFile('Select the glyphOrder file (.txt or .glyphorder):',
                     fileTypes=['glyphorder', 'enc', 'txt'])
    if goFile:
        goFilePath = goFile[0]
        glyphOrderList = []
        with open(goFilePath) as rf:
            goData = rf.read()
            lines = [line for line in goData.split(
                '\n') if line and line[0] != '#']
            for line in lines:
                glyphOrderList += line.split()
    else:
        return
    return glyphOrderList


def addGlyphsToFont(glyphs, font):
    font.disableUpdateInterface()
    notInFont = [gname for gname in glyphs if gname not in font.glyphs]
    print('\t-- Glyph names in glyphOrder and *not* in font: %s' % notInFont)
    for gname in notInFont:
        newGlyph = GSGlyph(gname)
        font.glyphs.append(newGlyph)
        newGlyph.updateGlyphInfo()
        print('\t\t-- Created: %s' % gname)
    gfont.enableUpdateInterface()

if __name__ == '__main__':
    glyphorder = getGlyphOrderFile()
    for gfont in Glyphs.fonts:
        gfont.disableUpdateInterface()
        print('-' * 17)
        gfont.customParameters['glyphOrder'] = glyphorder
        print('\t-- set glyphOrder and template to {0}'.format(gfont))
        # addGlyphsToFont(glyphorder, gfont)
        print('')
        print('Set glyphOrder to all open fonts and masters')
        print('')
        gfont.enableUpdateInterface()

Glyphs.showMacroWindow()
