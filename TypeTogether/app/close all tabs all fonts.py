# MenuTitle: Close all tabs all fonts

__doc__ = """\
Closes all tabs in all fonts view.
"""


for gfont in Glyphs.fonts:
    tabs = gfont.tabs

    while tabs:
        tabs[0].close()
