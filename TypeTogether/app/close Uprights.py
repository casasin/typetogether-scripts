# MenuTitle: Close Uprights

__doc__ = """\
Closes all fonts wich doesn't contain 'Italic', 'Oblique' or 'Slanted' in filepath.
"""

import os


slant = ['Italic', 'Oblique', 'Slanted']

all_gf = Glyphs.fonts
for gf in all_gf:
    if any(word not in os.path.basename(gf.filepath) for word in slant):
        gf.close()
