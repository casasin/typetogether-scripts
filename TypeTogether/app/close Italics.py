# MenuTitle: Close Italics

__doc__ = """\
Closes all fonts wich contain 'Italic', 'Oblique' or 'Slanted' in filepath.
"""

import os


slant = ['Italic', 'Oblique', 'Slanted']

all_gf = Glyphs.fonts
for gf in all_gf:
    if any(word in os.path.basename(gf.filepath) for word in slant):
    # if 'Italic' in :
        gf.close()
