# MenuTitle: Close all tabs

__doc__ = """\
Closes all tabs in current font view.
"""


gfont = Glyphs.font
tabs = gfont.tabs

while tabs:
    tabs[0].close()
