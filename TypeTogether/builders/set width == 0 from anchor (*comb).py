# MenuTitle: Set *comb width == 0 from anchor

__doc__ = """\
Sets selected glyphs width to 0 using the firt found of "_top, _bottom, _right, _center, _cedilla" anchors as reference for positioning.
Works only for *comb accents already existing in the font. It filters if 'comb' is not in the name and then it won't apply any change and report.
"""


def setWidthToZeroFromAnchor(glyph):
    for layer in glyph.layers:
        hasTop, hasBottom = False, False

        anchorNames = {anchor.name: anchor for anchor in layer.anchors}
        if '_top' in anchorNames.keys():
            aname = '_top'
        elif '_bottom' in anchorNames.keys():
            aname = '_bottom'
        elif '_right' in anchorNames.keys():
            aname = '_right'
        elif '_center' in anchorNames.keys():
            aname = '_center'
        elif '_cedilla' in anchorNames.keys():
            aname = '_cedilla'
        elif '_ogonek' in anchorNames.keys():
            aname = '_ogonek'
        else:
            aname = None
            print('-- %s has no _top/_bottom/_right/_center/_cedilla/_ogonek anchors, it will be centered' % glyph.name)
        if aname:
            a = anchorNames[aname]
            x = -a.position.x
        else:
            l, r = layer.bounds.origin.x, layer.bounds.size.width
            x = ((r - l) / 2) - r

        layer.applyTransform([1, 0, 0, 1, x, 0])
        layer.width = 0


# run as main
if __name__ == '__main__':
    gfont = Glyphs.font
    combSelected = [glyph for glyph in gfont.selection if 'comb' in glyph.name]

    for glyph in combSelected:
        setWidthToZeroFromAnchor(glyph)

    tabString = '/%s' % '/'.join([g.name for g in combSelected])
    gfont.newTab(tabString)
    # Glyphs.showMacroWindow()
