# MenuTitle: Build nbspace CR .null


__doc__ = """
   Build nbspace, CR and .null glyphs in AllFonts or ensure they have the correct width if  already existed.
"""

glyphs2build = ['.null', 'CR', 'nbspace']

for font in Glyphs.fonts:
    # add glyphs if they don't exist in the font
    for gname in glyphs2build:
        if gname not in font.glyphs:
            font.glyphs.append(GSGlyph(gname))

    # set CR and nbspace width == space width
    for i in range(len(font.glyphs['space'].layers)):
        print(font.glyphs['space'].layers[i])
        w = font.glyphs['space'].layers[i].width
        font.glyphs['nbspace'].layers[i].width = w
        font.glyphs['CR'].layers[i].width = w

    # set .null glyph width == 0
    for i in range(len(font.glyphs['.null'].layers)):
        font.glyphs['.null'].layers[i].width = 0
    print('Set /.null /CR /nbspace width in %s\n\n\t--' % font)
    print('')

Glyphs.showMacroWindow()
