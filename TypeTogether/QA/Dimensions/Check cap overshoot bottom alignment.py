# MenuTitle: Check caps overshoot bottom alignment (top)

__doc__ = """\
    Check if caps overshoot aligns with O bottom across masters.
"""


from collections import OrderedDict


Glyphs.clearLog()
Gfont = Glyphs.font
print(Gfont.filepath)
print('-' * 57)

foundError = False

for master in Gfont.masters:
    id = master.id
    layer = Gfont.glyphs['O'].layers[id]
    bsbControlGlyph = layer.BSB
    diffCapHeight = ['O']
    for c in 'CGJSU':
        clayer = Gfont.glyphs[c].layers[id]
        
        checkBsb = clayer.BSB
        if checkBsb != bsbControlGlyph:
            print((checkBsb, bsbControlGlyph))
            diffCapHeight.append(c)
    if len(diffCapHeight) > 1:
        masterNameTab = ''
        for c in master.name:
            masterNameTab += '/' + c
        tabString = '%s/space/C/a/p/s/space/b/o/t/t/o/m\n' % masterNameTab + '/' + '/'.join(diffCapHeight)
        Gfont.newTab(tabString)
        # # print report in macro output
        print()
        print('-' * 57)
        print("Glyphs with different bottom y than 'O':")
        print('-' * 57)
        print(' '.join(diffCapHeight[1:]))
        print()

if foundError == False:
    print('All seems good with cap overshoot bottom alignment…')

Glyphs.showMacroWindow()

