# MenuTitle: Check ascender alignment

__doc__ = """\
    Check if ascenders align with top h stem across masters.
"""


from collections import OrderedDict


Glyphs.clearLog()
Gfont = Glyphs.font
print(Gfont.filepath)
print('-' * 57)

foundError = False

for master in Gfont.masters:
    id = master.id
    layer = Gfont.glyphs['h'].layers[id]
    tsbControlGlyph = layer.TSB
    diffAscenders = ['h']
    for c in 'bdkl':
        clayer = Gfont.glyphs[c].layers[id]
        checkTsb = clayer.TSB
        if checkTsb != tsbControlGlyph:
            diffAscenders.append(c)
    if len(diffAscenders) > 1:
        masterNameTab = ''
        for c in master.name:
            masterNameTab += '/' + c
        tabString = '%s/space/a/s/c/e/n/d/e/r/s\n' % masterNameTab + \
            '/' + '/'.join(diffAscenders)
        Gfont.newTab(tabString)
        # # print report in macro output
        print()
        print('-' * 57)
        print("Glyphs with different top y than 'h':")
        print('-' * 57)
        print(' '.join(diffAscenders[1:]))
        print()


# tGlyphs = [glyph for glyph in Gfont.glyphs if glyph.name.split(
#     '.')[-1] in tabExtensions]

# # use arbitrary reference, first glyph, first layer width.
# refWidth = tGlyphs[0].layers[0].width
# print('tab reference: %s %s %s' %
#     (tGlyphs[0], tGlyphs[0].layers[0], tGlyphs[0].layers[0].width))

# wrongWidths = []
# for glyph in tGlyphs:
#     for layer in glyph.layers:
#         if layer.width != refWidth:
#             wrongWidths.append((glyph.name, layer.width))

# # open new tab with wrong width glyphs
# wrongGlyphs = [gname for gname, width in wrongWidths]
# noDupswrongGlyphs = list(OrderedDict.fromkeys(wrongGlyphs))
# # print('/' + '/'.join(wrongGlyphs))
# if noDupswrongGlyphs:
# 	tabString = '/t/a/b/space/w/i/d/t/h/colon/\n' + '/' + '/'.join(noDupswrongGlyphs)
# 	Gfont.newTab(tabString)


Glyphs.showMacroWindow()
