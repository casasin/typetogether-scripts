# MenuTitle: Check caps overshoot alignment (top)

__doc__ = """\
    Check if caps overshoot aligns with O top across masters.
"""


from collections import OrderedDict


Glyphs.clearLog()
Gfont = Glyphs.font
print(Gfont.filepath)
print('-' * 57)

foundError = False

for master in Gfont.masters:
    id = master.id
    layer = Gfont.glyphs['O'].layers[id]
    tsbControlGlyph = layer.TSB
    diffCapHeight = ['O']
    for c in 'CGQS':
        clayer = Gfont.glyphs[c].layers[id]

        checkTsb = clayer.TSB
        if checkTsb != tsbControlGlyph:
            print((checkTsb, tsbControlGlyph))
            diffCapHeight.append(c)
    if len(diffCapHeight) > 1:
        foundError = True
        masterNameTab = ''
        for c in master.name:
            masterNameTab += '/' + c
        tabString = '%s/space/C/a/p/s/space/H/e/i/g/h/t\n' % masterNameTab + \
            '/' + '/'.join(diffCapHeight)
        Gfont.newTab(tabString)
        # # print report in macro output
        print()
        print('-' * 57)
        print("Glyphs with different top y than 'O':")
        print('-' * 57)
        print(' '.join(diffCapHeight[1:]))
        print()

if foundError == False:
    print('All seems good with cap overshoot alignment…')

Glyphs.showMacroWindow()
