# MenuTitle: Check descender alignment

__doc__ = """\
    Check if descender align with bottom 'q' stem across masters.
"""


Glyphs.clearLog()
Gfont = Glyphs.font
print(Gfont.filepath)
print('-' * 57)

foundError = False

for master in Gfont.masters:
    id = master.id
    layer = Gfont.glyphs['q'].layers[id]
    bsbControlGlyph = layer.BSB
    diffDescenders = ['q']
    for c in 'p':
        clayer = Gfont.glyphs[c].layers[id]

        checkBsb = clayer.BSB
        if checkBsb != bsbControlGlyph:
            print((checkBsb, bsbControlGlyph))
            diffDescenders.append(c)
    if len(diffDescenders) > 1:
        masterNameTab = ''
        for c in master.name:
            masterNameTab += '/' + c
        tabString = '%s/space/d/e/s/c/e/n/d/e/r/s\n' % masterNameTab + \
            '/' + '/'.join(diffDescenders)
        tab = Gfont.newTab(tabString)
        tab.setMasterIndex_(Gfont.masters.index(master))
        # # print report in macro output
        print()
        print('-' * 57)
        print("Glyphs with different bottom y than 'q':")
        print('-' * 57)
        print(' '.join(diffDescenders[1:]))
        print()

if foundError == False:
    print('All seems good with cap descender alignment…')

Glyphs.showMacroWindow()
