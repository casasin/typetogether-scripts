# MenuTitle: Report components used and not exporting

__doc__ = """\
    Prints out glyphs used as components in other glyphs but set to 'not export'.
"""

Glyphs.clearLog()

for gf in Glyphs.fonts:
    # gf = Glyphs.font
    output = []
    print('')
    print(gf)

    for glyph in gf.glyphs:
        for layer in glyph.layers:
            for comp in layer.components:
                if comp.component.export is False and glyph.export is True:
                    output.append(u'⚠️ %s set to not export but is used as component in %s ' % (comp.componentName, glyph.name))
    if output:
        print('\n'.join(output))
    else:
        print('\n✅ No components used in the current font are set to "not export".')

    print('')
#
Glyphs.showMacroWindow()
