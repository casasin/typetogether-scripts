# MenuTitle: ⚠️ Check stem consistency 

__doc__ = """\
    Check stem width consistency across masters.
"""


from collections import OrderedDict


Glyphs.clearLog()
Gfont = Glyphs.font
print(Gfont.filepath)
print('-' * 57)

beamx, beamy = 0, 300

for master in Gfont.masters:
    id = master.id
    layer = Gfont.glyphs['i'].layers[id]
    intersections = layer.intersectionsBetweenPoints((beamx, beamy),
                                                     (layer.width, beamy),
                                                     components=False)
    x1, x2 = intersections[1].x, intersections[2].x
    stemWidth = x2 - x1
    print('stem width: %s' % stemWidth)
    # diffAscenders = ['h']
    # for c in 'bdkl':
    #     clayer = Gfont.glyphs[c].layers[id]
    #     checkTsb = clayer.TSB
    #     if checkTsb != tsbControlGlyph:
    #         diffAscenders.append(c)
    # if len(diffAscenders) > 1:
    #     masterNameTab = ''
    #     for c in master.name:
    #         masterNameTab += '/' + c
    #     tabString = '%s/space/a/s/c/e/n/d/e/r/s\n' % masterNameTab + '/' + '/'.join(diffAscenders)
    #     Gfont.newTab(tabString)
    #     # # print report in macro output
    #     print()
    #     print('-' * 57)
    #     print("Glyphs with different top y than 'h':")
    #     print('-' * 57)
    #     print(' '.join(diffAscenders[1:]))
    #     print()

Glyphs.showMacroWindow()
