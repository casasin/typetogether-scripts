# MenuTitle: Check extreme points

__doc__ = """\
    Checks if glyphs in all open fonts need extreme points to be added.
    Opens a tab with marked Glyphs.

    Save all the files first and run the script.
"""

from vanilla.dialogs import askYesNoCancel


Glyphs.clearLog()

def checkExtremePoints(font):
    glyphModifiedStamp = {glyph: glyph.lastChange for glyph in font.glyphs}

    for glyph, timestamp in glyphModifiedStamp.items():
        for layer in glyph.layers:
            layer.addNodesAtExtremes()
    afterChanges = {glyph: glyph.lastChange for glyph in font.glyphs}
    needExtremePoints = set(glyphModifiedStamp.items()) - set(afterChanges.items())
    glyphNamesForTab = [glyph.name for glyph in dict(needExtremePoints).keys()]
    glyphNamesForTab.sort()
    print('%s Need some points at extrema:\n-- %s' % (font, ' '.join(glyphNamesForTab)))
    tabString = '/X/t/r/e/m/e\n%s' % ('/' + '/'.join(glyphNamesForTab))
    font.newTab(tabString)


yes = askYesNoCancel(
    'All fonts should be saved before running this script.\nDo you want to proceed?')

if yes == 1:
    for gfont in Glyphs.fonts:
        checkExtremePoints(gfont)
    for gdoc in Glyphs.documents:
        gdoc.revertDocumentToSaved_(None)

Glyphs.showMacroWindow()
