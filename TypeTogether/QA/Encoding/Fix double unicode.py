# MenuTitle: Fix double unicode

__doc__ = """\
    Removes double or more unicode points in glyphs and updateGlyphInfo (get the right one). Don't save the font.
    Check the report and Save or not.
"""


cfont = Glyphs.font
print(f"{cfont}\n-------\n")

encoded = [g for g in cfont.glyphs if g.unicode]
for glyph in encoded:
    if len(glyph.unicodes) > 1:
        d_uni = list(glyph.unicodes)
        glyph.selected = True
        glyph.unicodes = None
        glyph.updateGlyphInfo()
        print(f'{d_uni} --> {list(glyph.unicodes)} : {glyph.name}')

msg = "\n--- 👉🏼👉🏼 Removed duplicated unicodes (see output above).\nSave the file if all OK. Revert the file if not."
print(msg)
Glyphs.showMacroWindow()
