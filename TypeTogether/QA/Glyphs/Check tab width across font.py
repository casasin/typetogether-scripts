# MenuTitle: Check tab width across font

__doc__ = """\
    Check if tab width is consistent across current font file.
    Checks glyphs wich name ends with ".tf" or ".tosf".
    Takes one tab glyph from front font as reference.
"""


from collections import OrderedDict


tabExtensions = ['tf', 'tosf']

Glyphs.clearLog()
Gfont = Glyphs.font
tGlyphs = [glyph for glyph in Gfont.glyphs if glyph.name.split(
    '.')[-1] in tabExtensions]

# use arbitrary reference, first glyph, first layer width.
refWidth = tGlyphs[0].layers[0].width
print('tab reference: %s %s %s' %
    (tGlyphs[0], tGlyphs[0].layers[0], tGlyphs[0].layers[0].width))

wrongWidths = []
for glyph in tGlyphs:
    for layer in glyph.layers:
        if layer.width != refWidth:
            wrongWidths.append((glyph.name, layer.width))

# open new tab with wrong width glyphs
wrongGlyphs = [gname for gname, width in wrongWidths]
noDupswrongGlyphs = list(OrderedDict.fromkeys(wrongGlyphs))
# print('/' + '/'.join(wrongGlyphs))
if noDupswrongGlyphs:
	tabString = '/t/a/b/space/w/i/d/t/h/colon/\n' + '/' + '/'.join(noDupswrongGlyphs)
	Gfont.newTab(tabString)

# # print report in macro output
print()
print('-' * 57)
print('Tab glyphs with inconsistent width:')
print('-' * 57)
print(' '.join(noDupswrongGlyphs))
print()
Glyphs.showMacroWindow()
# for gname, width in wrongWidths:
#     print('* {} == {} (reference width={})'.format(gname, width, refWidth))
# print()
