# MenuTitle: Check width from base

__doc__ = """\
    Checks if glyphs with diacritics have the same width than the base one.
    Runs on the current front document.
    Works with MM files too.

    tolerance is set to 4/em, so it won't report differences < 4/em
"""

from together.data.equivalents import bothEq


gfont = Glyphs.font
longestLayerName = max([len(layer.name) for layer in gfont.glyphs['A'].layers])

Glyphs.clearLog()
print()
print("Checking related widths")
print(gfont)
print('-' * 57)

# tolerance in /em units
tolerance = 4

for eq in bothEq:
	control, others = eq.split(' ')[0], eq.split(' ')[1:]
	controlGlyph = gfont.glyphs[control]
	eqReport = []
	for gname in others:
		checkGlyph = gfont.glyphs[gname]
		if checkGlyph and checkGlyph.export is True:
			if controlGlyph:
				# for master in gfont.masters:
				for layer in controlGlyph.layers:
					if checkGlyph.layers[layer.layerId] and checkGlyph.layers[layer.layerId]:
						controlwidth, checkwidth = int(layer.width), int(checkGlyph.layers[layer.layerId].width)
						# if controlwidth != checkwidth:
						if checkwidth > controlwidth + tolerance or checkwidth < controlwidth - tolerance:
						# if int(controlwidth) not in range(int(checkwidth - tolerance), int(checkwidth + tolerance)):
							checkReport = '%s :  %s  %s != %s  %s' % (layer.name.ljust(longestLayerName), controlGlyph.name, controlwidth, checkwidth, checkGlyph.name)
							eqReport.append(checkReport)
					else:
						pass
	if eqReport:
		print('\n'.join(eqReport))
	# else:
	# 	print("It seems there's nothing to report…")

Glyphs.showMacroWindow()
