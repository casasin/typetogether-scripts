# MenuTitle: Show interpolation usual suspects

__doc__ = """\
    Shows glyphs wich are usual suspects to have interpolation errors not easily spotable in Glyphs.app due to contour order in a new tab.
"""

gf = Glyphs.font
tabString = u'$€¢£¥₺%‰#‡№¤½⅓⅔¼¾⅛⅜⅝⅞≈≠≤≥=±÷'
gf.newTab(tabString)
