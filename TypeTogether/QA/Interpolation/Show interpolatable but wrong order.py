# MenuTitle: ⛔️ Show interpolatable but wrong order 01

__doc__ = """\
    Searches for possible interpolation issues due to interpolatable paths but in different order not reported by Glyphs.
    It reports suspicious positioning but it really depends in the design and number of masters.
    Doesn't check components order, should it?
"""


def checkHpos(layer):
    pathPos = []
    for path in layer.paths:
        x, y = path.bounds.origin.x, path.bounds.origin.y
        pathPos.append((x, y, layer.paths.index(path)))
    pathPos.sort()
    # return(pathPos)
    pathsSortedIndex = [i[2] for i in pathPos]
    return(pathsSortedIndex)


def checkVpos(layer):
    pathPos = []
    for path in layer.paths:
        y, height = path.bounds.origin.y, path.bounds.size.height
        pathPos.append((y + height, layer.paths.index(path)))
    pathPos.sort()
    # return(pathPos)
    pathsSortedIndex = [i[1] for i in pathPos]
    return(pathsSortedIndex)


gf = Glyphs.font
tabString = ''
glyphsToExport = [gglyph for gglyph in gf.glyphs if gglyph.export is True]

Glyphs.clearLog()

for gglyph in gf.glyphs:
    firstLayer = gglyph.layers[gf.masters[0].id]
    if len(firstLayer.paths) > 1:
        x_pathsOrder = checkHpos(firstLayer)
        height_pathsOrder = checkVpos(firstLayer)
        # print(gglyph.name, x_pathsOrder, firstLayer.name)
        for master in gf.masters[1:]:
            masterLayer = gglyph.layers[master.id]
            x_otherPathsOrder = checkHpos(masterLayer)
            height_otherPathsOrder = checkVpos(masterLayer)
            if x_otherPathsOrder != x_pathsOrder or height_pathsOrder != height_otherPathsOrder:
                tabString += '/%s' % gglyph.name
                print(u'⚠️? %s: review' % gglyph.name)
                break
if tabString:
    gf.newTab(tabString)

Glyphs.showMacroWindow()
