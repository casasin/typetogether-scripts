# MenuTitle: Copy Basic Info to ALL

__doc__ = """\
    Copies all basic font info from the front font to All other opened fonts. And sets some info to all from the script (licenseURL, vendor, version string) than can't be copied from the current font.
"""

# "properties" than must be set this way (doh)
propertiesDict = {
    "licenseURL": "http://www.type-together.com/resources/eula/TT-EULA.pdf",
    "vendorID": "TT",
    # "Version String": "Version %d.%03d"
}

cf = Glyphs.font

cf.disableUpdateInterface()
Glyphs.clearLog()


for otherFont in Glyphs.fonts[1:]:
    otherFont.familyName = cf.familyName
    #
    otherFont.versionMajor = cf.versionMajor
    otherFont.versionMinor = cf.versionMinor
    otherFont.designer = cf.designer
    otherFont.designerURL = cf.designerURL
    otherFont.manufacturer = cf.manufacturer
    otherFont.manufacturerURL = cf.manufacturerURL
    otherFont.copyright = cf.copyright
    otherFont.license = cf.license
    otherFont.trademark = cf.trademark
    otherFont.description = cf.description
    # set properties
    for k, val in propertiesDict.items():
        otherFont.setProperty_value_languageTag_(k, val, None)

for k, val in propertiesDict.items():
    cf.setProperty_value_languageTag_(k, val, None)


cf.enableUpdateInterface()
Glyphs.showMacroWindow()
