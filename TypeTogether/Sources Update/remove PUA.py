# MenuTitle: remove PUA all glyphs all fonts

__doc__ = """\
    Removes all Private unicodes in current font.
    PUA = range(U+E000 – U+F8FF)
"""

Glyphs.clearLog()

# build PUA range
puaRange = range(int('E000', 16), int('F8FF', 16))

for gf in Glyphs.fonts:
    gf.disableUpdateInterface()
    for g in gf.glyphs:
        if g.unicode is not None and int(g.unicode, 16) in puaRange:
            g.unicode = None
            print('Had PUA /%s, set to None' % g.name)
    gf.enableUpdateInterface()

Glyphs.showMacroWindow()
