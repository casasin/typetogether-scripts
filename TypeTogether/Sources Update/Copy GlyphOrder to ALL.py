# MenuTitle: Copy glyphOrder to ALL

__doc__ = """\
    Set glyphOrder from current font to ALL open fonts.
"""

import os


cfont = Glyphs.font
fonts = Glyphs.fonts[1:]
for gf in fonts:
    gf.customParameters["glyphOrder"] = cfont.customParameters["glyphOrder"]
    print(f"Set glyphOrder from current font to {os.path.basename(gf.filepath)}")
