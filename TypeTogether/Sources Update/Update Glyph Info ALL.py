# MenuTitle: Update Glyph Info ALL

__doc__ = """\
    Updates Glyph Info in ALL glyphs of ALL fonts.
"""


allFonts = Glyphs.fonts

for myFont in allFonts:
    myFont.disableUpdateInterface()
    for glyph in myFont.glyphs:
        glyph.updateGlyphInfo()
    myFont.enableUpdateInterface()
