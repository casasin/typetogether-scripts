# MenuTitle: Remove width from Master names

__doc__ = """
Removes width name references from master name as it should already be in the familyName"""

allFonts = Glyphs.fonts

widths = [
    "Compressed",
    "Condensed",
    "Extended",
]


def removeWidthFromMaster(font):
    for w in widths:
        for master in font.masters:
            if w in master.name:
                newname = master.name.replace(f"{w} ", "")
                print(f"{newname} Removed '{w}' from master.name")
                master.name = newname

for font in allFonts:
    removeWidthFromMaster(font)
