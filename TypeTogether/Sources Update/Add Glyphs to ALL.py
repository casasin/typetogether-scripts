# MenuTitle: Add Glyphs to ALL

__doc__ = """\
    Adds selected glyphs in the front font to all others.
"""


Glyphs.clearLog()
currentFont = Glyphs.font

selectedGlyphNames = [g.name for g in currentFont.selection]

for gfont in Glyphs.fonts[1:]:
    for gname in selectedGlyphNames:
        if gname not in gfont.glyphs:
            gfont.glyphs.append(GSGlyph(gname))
            print(f"{gname} added to {gfont}")
        else:
            print(f"{gname} already in {gfont}")

Glyphs.showMacroWindow()
