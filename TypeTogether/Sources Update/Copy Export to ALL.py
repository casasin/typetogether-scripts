# MenuTitle: Copy Not Export to ALL

__doc__ = """\
    Sets to not export all not exporting glyphs in front font to all fonts.
"""


cf = Glyphs.font
doNotExport = [g.name for g in cf.glyphs if g.export is False] + ["pff"]

print(doNotExport)

for font in Glyphs.fonts[1:]:
    for gname in doNotExport:
        if gname in font.glyphs:
            font.glyphs[gname].export = False

print("Copied not exporting glyphs from front font to all others!")
