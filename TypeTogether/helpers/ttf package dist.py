# MenuTitle: TTF Package Maker

__doc__ = """\
IMPORTANT: File names MUST be formatted 'FamilyName-StyleName.ttf'.

Zips all ttf fonts in a folder per weight and all together in a bundle
asks for a .pdf specimen if you want to add it to every font and bundle.

Saves the whole folder one directory up.
"""

# TODO: bundle the .zip actions in a function

import os
import os.path
import shutil
import zipfile
import zlib
from vanilla.dialogs import getFolder, getFile

# select the folder with the ttf fonts
fsel = getFolder(
    "Select folder containing all truetype fonts (.ttf) all together:")
if fsel:
    fpath = fsel[0]

pdf = getFile(
    "Select a specimen (PDF) file if you want it to be zipped with each ttf file  [CANCEL if you don't want to include the speciment file]:")
if pdf:
    pdfFilePath = pdf[0]
    pdfFileName = os.path.basename(pdfFilePath)

# list source dir and get font files by extension
# ttfs = getFolder(fpath, xt='.ttf')
ttfs = [os.path.join(fpath, ttf)
        for ttf in os.listdir(fpath) if ttf.endswith('.ttf') is True]

print(ttfs)
familyName = os.path.basename(ttfs[0]).split('-')[0]
print(familyName)

# make target dir
targetFolder = '%s-ttf-package' % familyName
prevf = os.path.dirname(fpath)
targetFolderPath = os.path.join(prevf, targetFolder)
# remove target folder if exists and make new one
try:
    shutil.rmtree(targetFolderPath)
except OSError:
    pass
finally:
    os.mkdir(targetFolderPath)


# zip each ttf
for ttf in ttfs:
    ttfFileName = os.path.basename(ttf)
    root, xt = ttfFileName.split('.')
    familyName, styleName = root.split('-')
    zipfpath = os.path.join(
        targetFolderPath, f'{familyName}-{styleName}.zip')
    zipf = zipfile.ZipFile(zipfpath, 'w', zipfile.ZIP_DEFLATED)
    # write only the file, not the whole path to zipped file
    zipf.write(ttf, ttfFileName)
    # add specimen
    if pdf:
	    zipf.write(pdfFilePath, pdfFileName)
    zipf.close()
# make ttf bundle
zipBundlePath = os.path.join(
    targetFolderPath, f'{familyName}_Bundle.zip')
zipBundle = zipfile.ZipFile(zipBundlePath, 'w', zipfile.ZIP_DEFLATED)
for ttf in ttfs:
    zipBundle.write(ttf, os.path.basename(ttf))
if pdf:
	zipBundle.write(pdfFilePath, pdfFileName)
zipBundle.close()

os.system('open %s' % targetFolderPath)
