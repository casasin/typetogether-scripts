# MenuTitle: DESKTOP Package Maker (OTF+TTF)

__doc__ = """\
Zips all otf and ttf fonts in a folder per style and all together in a bundle.
Asks for a .pdf specimen if you want to add it to every zipped file.
Saves the whole folder one directory up.

IMPORTANT: File names MUST be formatted 'FamilyName-StyleName.[otf/ttf]'.
"""

# TODO: bundle the .zip actions in a function

import os
import os.path
import shutil
import zipfile
import zlib
from vanilla.dialogs import getFolder, getFile


def getFamilynameFromFilename(fontFilePath):
    __doc__ = """
    Needs 'FamilyName-StyleName.ext' file name formatting.
    """
    familyName = os.path.basename(fontFilePath.split('-')[0])
    return familyName


def getSameBasename(fontPaths):
    __doc__ = """
    Returns a dictionary with basenames minus their extension as key and the whole filepaths with the same file name minus the extension (otf/ttf).
    """
    sameBasename = {}
    for fontPath in fontPaths:
        root, xt = os.path.basename(fontPath).split('.')
        if root not in sameBasename.keys():
            sameBasename[root] = [fontPath]
        else:
            sameBasename[root] += [fontPath]
    return sameBasename


def checkPairedFonts(baseDict):
    __doc__ = """

    """
    l = len(list(baseDict.values())[0])
    for val in list(baseDict.values()):
        if len(val) != l:
            print(f"👎🏼 ERROR pairing fonts {val}")
            return
        else:
            continue
    print("👍🏽 Fonts properly paired\n")


def checkSubfamily(baseDict):
    __doc__ = """

    """
    familyNames = set([base.split('-')[0] for base in baseDict.keys()])
    if len(familyNames) > 1:
        return familyNames
    else:
        return None


# Do stuff:
Glyphs.clearLog()

# select the folders with the otf and ttf fonts
fonts_folders = getFolder(
    "Select the folders containing all fonts you want to zip (.otf/.ttf):",
    allowsMultipleSelection=True)
if fonts_folders:
    folder_paths = list(fonts_folders)

# get all otf and ttf fonts from the selected folders
all_fonts = []
ext = ('.otf', '.ttf')

for fpath in folder_paths:
    for files in os.listdir(fpath):
        if files.endswith(ext):
            all_fonts.append(os.path.join(fpath, files))

all_fonts.sort()
otf_fonts = [f for f in all_fonts if f.endswith('.otf')]
ttf_fonts = [f for f in all_fonts if f.endswith('.ttf')]

# make dictionary
allfonts_dict = getSameBasename(all_fonts)
checkPairedFonts(allfonts_dict)
subfamilies = checkSubfamily(allfonts_dict)

pdf = getFile(
    "Select a PDF specimen file if you want it to be zipped with each package file, [CANCEL] if you don't want to include any specimen file:")
if pdf:
    pdfFilePath = pdf[0]
    pdfFileName = os.path.basename(pdfFilePath)

# make target dir from first font in all_fonts list of font filepaths
familyName = os.path.basename(all_fonts[0]).split('-')[0]
targetFolderName = f'{familyName}-desktop-packages'
upper_folder = os.path.dirname(folder_paths[0])
targetFolderPath = os.path.join(upper_folder, targetFolderName)
# remove target folder if exists and make new one
try:
    shutil.rmtree(targetFolderPath)
except OSError:
    pass
finally:
    os.mkdir(targetFolderPath)

# zip each otf with its ttf companion all together
for k, val in allfonts_dict.items():
    zipfpath = os.path.join(targetFolderPath, f'{k}.zip')
    zipf = zipfile.ZipFile(zipfpath, 'w', zipfile.ZIP_DEFLATED)
    for fontpath in val:
        zipf.write(fontpath, os.path.basename(fontpath))
        # add specimen
        if pdf:
            zipf.write(pdfFilePath, pdfFileName)
    zipf.close()

# make otfs+ttfs bundle ALL TOGETHER
zipBundlePath = os.path.join(
    targetFolderPath, f'{familyName}_Bundle-ALL.zip')
zipBundle = zipfile.ZipFile(zipBundlePath, 'w', zipfile.ZIP_DEFLATED)
for otf in otf_fonts:
    zipBundle.write(otf, f'OTF/{os.path.basename(otf)}')
for ttf in ttf_fonts:
    zipBundle.write(ttf, f'TTF/{os.path.basename(ttf)}')
if pdf:
    zipBundle.write(pdfFilePath, pdfFileName)
zipBundle.close()

# make otfs+ttfs bundle SUB-FAMILIES (like Portada/PortadaText)
if subfamilies:
    sub_dict = {sub: [] for sub in subfamilies}
    for font in all_fonts:
        family = os.path.basename(font).split('-')[0]
        if family in sub_dict.keys():
            sub_dict[family] += [font]
        else:
            print(f'WRONG, something with {font}')
    for subfamily in sub_dict.keys():
        zipBundlePath = os.path.join(
            targetFolderPath, f'{subfamily}_Bundle.zip')
        zipBundle = zipfile.ZipFile(zipBundlePath, 'w', zipfile.ZIP_DEFLATED)
        for fpath in sub_dict[subfamily]:
            xt = fpath.split('.')[-1]
            zipBundle.write(fpath, f'{xt.upper()}/{os.path.basename(fpath)}')
        if pdf:
            zipBundle.write(pdfFilePath, pdfFileName)
        zipBundle.close()

os.system(f'open {targetFolderPath}')

Glyphs.showMacroWindow()
