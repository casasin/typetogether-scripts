# MenuTitle: WEBFONTS package maker

__doc__ = """\
IMPORTANT: File names MUST be formatted 'FamilyName-StyleName.xxx'.

Zips all woff, woff2, eot and svg fonts in a folder per weight and all together in a bundle.

Saves the whole folder one directory up.
"""

# TODO: bundle the .zip actions in a function

import os
import os.path
import shutil
import zipfile
import zlib
from vanilla.dialogs import getFolder, getFile

# select the folder with the webfonts
fsel = getFolder('Select folder containing all webfonts all together:')
if fsel:
    fpath = fsel[0]

# webfont formats to be packed
wfXt = ['.eot', '.svg', '.woff', '.woff2']
wfDict = {}
webfonts = [path for path in os.listdir(
    fpath) if os.path.splitext(path)[1] in wfXt]
for path in webfonts:
    root, xt = os.path.basename(path).split('.')
    if root in wfDict.keys():
        wfDict[root].append(path)
    else:
        wfDict[root] = [path]

# make target dir
prevf = os.path.dirname(fpath)
selecf = os.path.split(fpath)[-1]
print(prevf, selecf)
targetFolderName = u'%s-web-package' % selecf
targetFolderPath = os.path.join(prevf, targetFolderName)
print(targetFolderPath)
# remove target folder if exists and make new one
try:
    shutil.rmtree(targetFolderPath)
except OSError:
    pass
finally:
    os.mkdir(targetFolderPath)
print(u'– targetFolder: %s' % targetFolderPath)

# zip bundle
# use last
familyName = root.split('-')[0]
bundleName = f'{familyName}_Bundle-web.zip'
print(u'– bundleName: %s' % bundleName)
allwf = []
# get all webfonts
for formats in wfDict.values():
    allwf += formats
allwf.sort()
print(allwf)

# create zip file
zipBundlePath = os.path.join(targetFolderPath, bundleName)
zipBundle = zipfile.ZipFile(zipBundlePath, 'w', zipfile.ZIP_DEFLATED)
for wf in allwf:
    zipBundle.write(os.path.join(fpath, wf), wf)
    # print(os.path.join(fpath, wf), wf)
zipBundle.close()

# zip each weight alone
for k in wfDict.keys():
    print(k)
    # zipFileName = u'{0}-web.zip'.format(k.replace('-', '_'))
    zipFileName = f'{k}-web.zip'
    zipWebfontPath = os.path.join(targetFolderPath, zipFileName)
    print(k, wfDict[k])
    zipFont = zipfile.ZipFile(zipWebfontPath, 'w', zipfile.ZIP_DEFLATED)
    for wf in wfDict[k]:
        zipFont.write(os.path.join(fpath, wf), wf)
    zipFont.close()

#
Glyphs.showMacroWindow()
# open target folder
os.system('open %s' % zipBundlePath)
