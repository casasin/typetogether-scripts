# MenuTitle: OTF Package Maker

__doc__ = """\
Zips all otf fonts in a folder per weight and all together in a bundle
asks for a .pdf specimen if you want to add it to every font and bundle.
Saves the whole folder one directory up.

IMPORTANT: File names MUST be formatted 'FamilyName-StyleName.otf'.
"""

# TODO: bundle the .zip actions in a function

import os
import os.path
import shutil
import zipfile
import zlib
from vanilla.dialogs import getFolder, getFile

# select the folder with the otf fonts
fsel = getFolder(
    'Select folder containing all desktop fonts (.otf) all together:')
if fsel:
    fpath = fsel[0]

pdf = getFile(
    "Select a specimen (PDF) file if you want it to be zipped with each otf file [CANCEL if you don't want to include the speciment file]:")
if pdf:
    pdfFilePath = pdf[0]
    pdfFileName = os.path.basename(pdfFilePath)

# list source dir and get font files by extension
# otfs = getFolder(fpath, xt='.otf')
otfs = [os.path.join(fpath, otf)
        for otf in os.listdir(fpath) if otf.endswith('.otf') is True]

print(otfs)
familyName = os.path.basename(otfs[0]).split('-')[0]
print(familyName)

# make target dir
targetFolder = '%s-otf-package' % familyName
prevf = os.path.dirname(fpath)
targetFolderPath = os.path.join(prevf, targetFolder)
# remove target folder if exists and make new one
try:
    shutil.rmtree(targetFolderPath)
except OSError:
    pass
finally:
    os.mkdir(targetFolderPath)


# zip each otf
for otf in otfs:
    otfFileName = os.path.basename(otf)
    root, xt = otfFileName.split('.')
    familyName, styleName = root.split('-')
    zipfpath = os.path.join(
        targetFolderPath, f'{familyName}-{styleName}.zip')
    zipf = zipfile.ZipFile(zipfpath, 'w', zipfile.ZIP_DEFLATED)
    # write only the file, not the whole path to zipped file
    zipf.write(otf, otfFileName)
    # add specimen
    if pdf:
	    zipf.write(pdfFilePath, pdfFileName)
    zipf.close()
# make otf bundle
zipBundlePath = os.path.join(
    targetFolderPath, f'{familyName}_Bundle.zip')
zipBundle = zipfile.ZipFile(zipBundlePath, 'w', zipfile.ZIP_DEFLATED)
for otf in otfs:
    zipBundle.write(otf, os.path.basename(otf))
if pdf:
	zipBundle.write(pdfFilePath, pdfFileName)
zipBundle.close()

os.system('open %s' % targetFolderPath)
